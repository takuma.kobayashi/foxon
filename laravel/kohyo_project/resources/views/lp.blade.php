<!DOCTYPE html>
<html>
	<head>
		<meta charaset='UTF-8'>
		<title>府奥村PRページ</title>
		<link href="{{ asset('/css/lp.css') }}" rel="stylesheet">
		<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
		<link href='https://fonts.googleapis.com/css?family=Anton' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Neucha' rel='stylesheet' type='text/css'>
		<link href="https://fonts.googleapis.com/css?family=M+PLUS+Rounded+1c" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Ravi+Prakash" rel="stylesheet">
		
	</head>
	<body>
		<div class="header">
			<div class="logo">
				<img alt="sonshou" src="{{ asset('/img/sonshou_clear.png')}}" style="widht: 60px; height: 60px;">
			</div>
			<div class="rightnav">
				<ul>
					<li><a href="#">トップ</a></li>
					<li><a href="#welcome">ようこそ府奥村へ</a></li>
					<li><a href="#living">住みやすさの追求</a></li>
					<li><a href="#challenge">府奥村に挑戦</a></li>
				</ul>
			</div>
			<div class="leftnav"></div>
		</div>
		<div class="container">
			<div class="content_head">
			<div class="plate">
				<p class="shadow text1">FOXON</p>
				<p class="shadow text1">CARD</p>
				<p class="shadow text1">BATTLE</p>
				<p class="shadow text1">STATION</p>
				<p class="script"><span>produce by KOHYO</span></p>
			</div>
			<div id="wrapper">
				<div id="slider-wrap">
					<ul id="slider">
						<li id="spring"></li>
						<li id="summer"></li>
						<li id="autum"></li>
						<li id="winter"></li>
					</ul>
						<!--controls-->
					<div class="btns" id="next"><i class="fa fa-arrow-right"></i></div>
					<div class="btns" id="previous"><i class="fa fa-arrow-left"></i></div>
					<div id="counter"></div>
					
					<div id="pagination-wrap">
						<ul></ul>
					</div>
					<!--controls-->   
				</div>
			</div>

			</div>
			<div class="content_1" id="welcome">
				<div class="plate">
					<p class="shadow text1 box">WELCOME</p>
					<p class="text_2 box_2" style="top: 70px; left: 225px;">天然のプラネタリウム</p>
					<p class="text_2 box_4" style="top: 160px; left: 610px;">広がる豊かな大自然</p>
					<p class="text_2 box_3" style="top: 250px; left: 370px;">人々のふれあい・温もり</p>
				</div>
			</div>
			<div class="content_2" id="living">
				<div class="plate" style="margin-top: 70px">
					<p class="shadow text1 box">LIVING</p>
				</div>
				<div class="content_2_bg">
					
					<div class="card">
						<p class="card_title">村長より</p>
						<p class="card_body">
							村民を家族とだとおっしゃる<br>
							府奥村の梅沢村長。<br>
							のどかでたくさんの自然に囲まれた村で<br>
							暮らす人々の心も<br>
							豊かであることが府奥村の自慢。
						</p>
					</div>
					<div class="card">
					<p class="card_title">子育て</p>
						<p class="card_body">
							村民を家族とだとおっしゃる<br>
							府奥村の梅沢村長。<br>
							のどかでたくさんの自然に囲まれた村で<br>
							暮らす人々の心も<br>
							豊かであることが府奥村の自慢。
						</p>
					</div>
					<div class="card">
					<p class="card_title">仕事</p>
						<p class="card_body">
							村民を家族とだとおっしゃる<br>
							府奥村の梅沢村長。<br>
							のどかでたくさんの自然に囲まれた村で<br>
							暮らす人々の心も<br>
							豊かであることが府奥村の自慢。
						</p>
					</div>
					<div class="card">
					<p class="card_title">食</p>
						<p class="card_body">
							村民を家族とだとおっしゃる<br>
							府奥村の梅沢村長。<br>
							のどかでたくさんの自然に囲まれた村で<br>
							暮らす人々の心も<br>
							豊かであることが府奥村の自慢。
						</p>
					</div>
				</div>
				
			</div>
			<div class="content_3" id="challenge">
				<div class="plate">
					<p class="shadow text1 box">CHALLENGE</p>
				</div>
				<section class="early">
					<article>
						<p class="example-right"> FOXON CARD BATTLE STATION FOXON CARD BATTLE STATION FOXON CARD BATTLE STATION FOXON CARD BATTLE STATION FOXON CARD BATTLE STATION FOXON CARD BATTLE STATION FOXON CARD BATTLE STATION FOXON CARD BATTLE STATION FOXON CARD BATTLE STATION FOXON CARD BATTLE STATION FOXON CARD BATTLE STATION FOXON CARD BATTLE STATION FOXON CARD BATTLE STATION FOXON CARD BATTLE STATION</p>
					</article>
					<article style="min-height: 4.5em;">
						<p class="example-left" style="font-size: 22px;color: #00d6fc;"> FOXON CARD BATTLE STATION FOXON CARD BATTLE STATION FOXON CARD BATTLE STATION FOXON CARD BATTLE STATION FOXON CARD BATTLE STATION FOXON CARD BATTLE STATION FOXON CARD BATTLE STATION FOXON CARD BATTLE STATION FOXON CARD BATTLE STATION FOXON CARD BATTLE STATION FOXON CARD BATTLE STATION</p>
						<p class="example-right" style="font-size: 30px;"> FOXON CARD BATTLE STATION FOXON CARD BATTLE STATION FOXON CARD BATTLE STATION FOXON CARD BATTLE STATION FOXON CARD BATTLE STATION FOXON CARD BATTLE STATION FOXON CARD BATTLE STATION FOXON CARD BATTLE STATION FOXON CARD BATTLE STATION FOXON CARD BATTLE STATION FOXON CARD BATTLE STATION FOXON CARD BATTLE STATION</p>
					</article>
					<article>
							<p class="example-left"style="margin-top: 10px;opacity: 0.4;"> FOXON CARD BATTLE STATION FOXON CARD BATTLE STATION FOXON CARD BATTLE STATION FOXON CARD BATTLE STATION FOXON CARD BATTLE STATION FOXON CARD BATTLE STATION FOXON CARD BATTLE STATION FOXON CARD BATTLE STATION FOXON CARD BATTLE STATION FOXON CARD BATTLE STATION FOXON CARD BATTLE STATION</p> 
					</article>
				</section>
				<div class="challenge_card">
					<p class="challenge_title">
						<span>府奥村</span>と<span>東京23区</span>の<br>
						壮絶な住みやすさ比較バトルがここに開幕！<br>
						君の街は府奥村に勝てるか...
					</p>
					<p class="challenge_info">※当サイトにおける比較・判定につきまして第三者委員会の公正な調査の元行っております。<br>ご意見・お問い合わせにつきましては下記アドレスまでお問い合わせ下さい。<br>カスタマーサービスセンター　<span>takuya.honjo@fox-hound.tech</span></p>
					<div class="challenge_btn"><a class="button" href="http://192.168.110.110/">いざ挑戦っ!!</a></div>
				</div>
				<section class="early">
					<article>
						<p class="example-right" style="font-size: 22px;"> FOXON CARD BATTLE STATION FOXON CARD BATTLE STATION FOXON CARD BATTLE STATION FOXON CARD BATTLE STATION FOXON CARD BATTLE STATION FOXON CARD BATTLE STATION FOXON CARD BATTLE STATION FOXON CARD BATTLE STATION FOXON CARD BATTLE STATION FOXON CARD BATTLE STATION FOXON CARD BATTLE STATION FOXON CARD BATTLE STATION</p>
					</article>
					<article>
						<p class="example-left" > FOXON CARD BATTLE STATION FOXON CARD BATTLE STATION FOXON CARD BATTLE STATION FOXON CARD BATTLE STATION FOXON CARD BATTLE STATION FOXON CARD BATTLE STATION FOXON CARD BATTLE STATION FOXON CARD BATTLE STATION FOXON CARD BATTLE STATION FOXON CARD BATTLE STATION FOXON CARD BATTLE STATION FOXON CARD BATTLE STATION FOXON CARD BATTLE STATION FOXON CARD BATTLE STATION FOXON CARD BATTLE STATION</p>
						<p class="example-right" style="color: #00d6fc; margin-top: 3px;"> FOXON CARD BATTLE STATION FOXON CARD BATTLE STATION FOXON CARD BATTLE STATION FOXON CARD BATTLE STATION FOXON CARD BATTLE STATION FOXON CARD BATTLE STATION FOXON CARD BATTLE STATION FUXON CARD BATTLE STATION FOXON CARD BATTLE STATION FOXON CARD BATTLE STATION</p>
					</article>
					<article>
							<p class="example-left" style="font-size: 22px;"> FOXON CARD BATTLE STATION FOXON CARD BATTLE STATION FOXON CARD BATTLE STATION FOXON CARD BATTLE STATION FOXON CARD BATTLE STATION FOXON CARD BATTLE STATION FOXON CARD BATTLE STATION FOXON CARD BATTLE STATION FOXON CARD BATTLE STATION FOXON CARD BATTLE STATION FOXON CARD BATTLE STATION FOXON CARD BATTLE STATION FOXON CARD BATTLE STATION</p> 
					</article>
				</section>
			</div>
		</div>
		<div class=footer>
		<script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>
			<script src="{{ asset('js/lp.js') }}" ></script>
		</div>
	</body>
</html>
